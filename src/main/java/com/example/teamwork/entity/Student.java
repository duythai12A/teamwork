package com.example.teamwork.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "student")
public class Student {
    @Id
    @Column( name = "id")
    private int id;
    @Column(name = "name")
    private String name;
    @Column(name = "classname")
    private String classname;
    @Column(name = "school")
    private String school;


}
