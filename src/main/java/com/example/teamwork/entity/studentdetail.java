package com.example.teamwork.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

public class studentdetail {



    @Entity
    @AllArgsConstructor
    @NoArgsConstructor
    @Table(name = "studentdetail")
    public class Studentdetail {
        @Id
        @Column(name = "idd")
        private Integer idd;

        @Column(name = "fullname")
        private String fullname;

        @Column(name = "address")
        private String address;

        @Column(name = "gender")
        private String gender;


    }

}
